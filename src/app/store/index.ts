import * as fromPosts from './posts/index';
export * from './posts/index'

export const reducers = {
    posts: fromPosts.postsReducer
}