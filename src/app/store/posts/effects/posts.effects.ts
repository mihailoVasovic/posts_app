import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, switchMap, catchError } from 'rxjs/operators';
import { ApiService } from '../../../core/api/api.service';
import * as actions from '../actions/api-posts.action';
import { Store } from '@ngrx/store';

@Injectable()
export class PostsEffects {
  @Effect()
  FetchPosts$ = this.actions$.pipe(
    ofType(actions.ApiPostsActions.FETCH),
    map((payload: actions.Fetch) => payload.payload),
    switchMap(payload =>
      this.service.fetchPosts(payload).pipe(
        map(posts => new actions.FetchSuccess(posts)),
        catchError(err => {
          alert('Something went wrong');
          return of(new actions.FetchFail());
        })
      )
    )
  );

  @Effect()
  FetchOnePost$ = this.actions$.pipe(
    ofType(actions.ApiPostsActions.FETCH_ONE),
    map((payload: actions.FetchOne) => payload.payload),
    switchMap(payload =>
      this.service.fetchPost(payload).pipe(
        map(post => new actions.FetchOneSuccess(post)),
        catchError(err => {
          alert('Something went wrong');
          return of(new actions.FetchOneFail());
        })
      )
    )
  );

  @Effect()
  SearchPosts$ = this.actions$.pipe(
    ofType(actions.ApiPostsActions.SEARCH),
    map((payload: actions.Search) => payload.payload),
    switchMap(payload =>
      this.service.fetchPosts(payload).pipe(
        map(
          posts => new actions.SearchSuccess({ posts, actionPayload: payload })
        ),
        catchError(err => {
          alert('Something went wrong');
          return of(new actions.SearchFail());
        })
      )
    )
  );

  @Effect()
  AddPost$ = this.actions$.pipe(
    ofType(actions.ApiPostsActions.ADD),
    map((payload: actions.Add) => payload.payload),
    switchMap(payload =>
      this.service.addPost(payload).pipe(
        map(post => {
          alert('Post added');
          this.store.dispatch(new actions.Reset());
          return new actions.AddSuccess(post);
        }),
        catchError(err => {
          alert('Something went wrong');

          return of(new actions.AddFail());
        })
      )
    )
  );

  @Effect()
  EditPost$ = this.actions$.pipe(
    ofType(actions.ApiPostsActions.EDIT),
    map((payload: actions.Edit) => payload.payload),
    switchMap(payload =>
      this.service.editPost(payload).pipe(
        map(post => {
          alert('Post updated');
          this.store.dispatch(new actions.Reset());
          return new actions.EditSuccess(post);
        }),
        catchError(err => {
          alert('Something went wrong');
          return of(new actions.EditFail());
        })
      )
    )
  );

  @Effect()
  RemovePost$ = this.actions$.pipe(
    ofType(actions.ApiPostsActions.REMOVE),
    map((payload: actions.Remove) => payload.payload),
    switchMap(payload =>
      this.service.removePost(payload).pipe(
        map(_ => {
          alert('Post removed');
          this.store.dispatch(new actions.Reset());
          return new actions.Fetch({ page: 1 });
        }),
        catchError(err => {
          alert('Something went wrong');
          return of(new actions.RemoveFail());
        })
      )
    )
  );

  constructor(
    private service: ApiService,
    private actions$: Actions,
    private store: Store<any>
  ) {}
}
