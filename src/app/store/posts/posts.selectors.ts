import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from './reducers/posts.reducer';

export const postState = createFeatureSelector<State>('posts');

export const posts = createSelector(
  postState,
  state => state.posts
);

export const post = createSelector(
  postState,
  state => state.post
);

export const loading = createSelector(
  postState,
  state => state.loading
);
