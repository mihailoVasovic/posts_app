import { Action } from '@ngrx/store';

export enum ApiPostsActions {
  FETCH = '{Posts} Fetch',
  FETCH_SUCCESS = '{Posts} Fetch Success',
  FETCH_FAIL = '{Posts} Fetch Fail',

  SEARCH = '{Posts} Search',
  SEARCH_SUCCESS = '{Posts} Search Success',
  SEARCH_FAIL = '{Posts} Search Fail',

  ADD = '{Posts} Add',
  ADD_SUCCESS = '{Posts} Add Success',
  ADD_FAIL = '{Posts} Add Fail',

  EDIT = '{Posts} Edit',
  EDIT_SUCCESS = '{Posts} Edit Success',
  EDIT_FAIL = '{Posts} Edit Fail',

  FETCH_ONE = '{Posts} Fetch One',
  FETCH_ONE_SUCCESS = '{Posts} Fetch One Success',
  FETCH_ONE_FAIL = '{Posts} Fetch One Fail',

  REMOVE = '{Posts} Remove',
  REMOVE_SUCCESS = '{Posts} Remove Success',
  REMOVE_FAIL = '{Posts} Remove Fail',

  RESET = '{Posts} Reset'
}

export class Fetch implements Action {
  readonly type = ApiPostsActions.FETCH;
  constructor(public payload) {}
}

export class FetchSuccess implements Action {
  readonly type = ApiPostsActions.FETCH_SUCCESS;
  constructor(public payload: any) {}
}

export class FetchFail implements Action {
  readonly type = ApiPostsActions.FETCH_FAIL;
  constructor() {}
}

export class Search implements Action {
  readonly type = ApiPostsActions.SEARCH;
  constructor(public payload) {}
}

export class SearchSuccess implements Action {
  readonly type = ApiPostsActions.SEARCH_SUCCESS;
  constructor(public payload: any) {}
}

export class SearchFail implements Action {
  readonly type = ApiPostsActions.SEARCH_FAIL;
  constructor() {}
}

export class Add implements Action {
  readonly type = ApiPostsActions.ADD;
  constructor(public payload) {}
}

export class AddSuccess implements Action {
  readonly type = ApiPostsActions.ADD_SUCCESS;
  constructor(public payload: any) {}
}

export class AddFail implements Action {
  readonly type = ApiPostsActions.ADD_FAIL;
  constructor() {}
}

export class Edit implements Action {
  readonly type = ApiPostsActions.EDIT;
  constructor(public payload) {}
}

export class EditSuccess implements Action {
  readonly type = ApiPostsActions.EDIT_SUCCESS;
  constructor(public payload: any) {}
}

export class EditFail implements Action {
  readonly type = ApiPostsActions.EDIT_FAIL;
  constructor() {}
}

export class Reset implements Action {
  readonly type = ApiPostsActions.RESET;
  constructor() {}
}

export class FetchOne implements Action {
  readonly type = ApiPostsActions.FETCH_ONE;
  constructor(public payload) {}
}

export class FetchOneSuccess implements Action {
  readonly type = ApiPostsActions.FETCH_ONE_SUCCESS;
  constructor(public payload: any) {}
}

export class FetchOneFail implements Action {
  readonly type = ApiPostsActions.FETCH_ONE_FAIL;
  constructor() {}
}

export class Remove implements Action {
  readonly type = ApiPostsActions.REMOVE;
  constructor(public payload) {}
}

export class RemoveSuccess implements Action {
  readonly type = ApiPostsActions.REMOVE_SUCCESS;
  constructor() {}
}

export class RemoveFail implements Action {
  readonly type = ApiPostsActions.REMOVE_FAIL;
  constructor() {}
}

export type Actions =
  | Fetch
  | FetchSuccess
  | FetchFail
  | Search
  | SearchSuccess
  | SearchFail
  | Add
  | AddSuccess
  | AddFail
  | Edit
  | EditSuccess
  | EditFail
  | Reset
  | FetchOne
  | FetchOneSuccess
  | FetchOneFail
  | Remove
  | RemoveFail
  | RemoveSuccess;
