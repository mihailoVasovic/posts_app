import { Actions, ApiPostsActions } from '../actions/api-posts.action';

export interface State {
  posts: {}[];
  post: {};
  loading: boolean;
}

export const initialState: State = {
  posts: [],
  post: null,
  loading: false
};

export function postsReducer(state: State = initialState, action: Actions) {
  switch (action.type) {
    case ApiPostsActions.FETCH:
    case ApiPostsActions.SEARCH:
    case ApiPostsActions.FETCH_ONE:
    case ApiPostsActions.EDIT:
      return {
        ...state,
        loading: true
      };
    case ApiPostsActions.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        posts: state.posts.concat(action.payload)
      };
    case ApiPostsActions.SEARCH_SUCCESS:
      let posts;
      if (action.payload.actionPayload.searchValue) {
        if (action.payload.actionPayload.page === 1) {
          posts = [...action.payload.posts];
        } else {
          posts = state.posts.concat(action.payload.posts);
        }
      }

      return {
        ...state,
        loading: false,
        posts
      };
    case ApiPostsActions.FETCH_FAIL:
      return {
        ...state,
        loading: false
      };

    case ApiPostsActions.ADD_SUCCESS:
      return {
        ...state,
        loading: false
      };

    case ApiPostsActions.EDIT_SUCCESS:
      return {
        ...state,
        loading: false
      };
    case ApiPostsActions.FETCH_ONE_SUCCESS:
      return {
        ...state,
        loading: false,
        post: action.payload
      };
    case ApiPostsActions.EDIT_SUCCESS:
      return {
        ...state,
        loading: false
      };
    case ApiPostsActions.EDIT_FAIL:
      return {
        ...state,
        loading: false
      };
    case ApiPostsActions.RESET:
      return {
        ...state,
        posts: []
      };

    default:
      return state;
  }
}
