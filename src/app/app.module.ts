import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import * as store from './store/index';

const MODULES = [SharedModule];

const REDUX = [
  StoreModule.forRoot(store.reducers),
  EffectsModule.forRoot([store.PostsEffects]),
  StoreDevtoolsModule.instrument({
    maxAge: 25,
    logOnly: false,
    features: {
      pause: false,
      lock: true,
      persist: true
    }
  })
]

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, ...MODULES, ...REDUX],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
