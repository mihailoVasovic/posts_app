import { Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { distinctUntilChanged } from 'rxjs/operators';
import { ValidationLabels } from '../forms/validation-labels';

export abstract class AbstractInputComponent implements OnInit {
  /**
   * @description formControlName
   */
  @Input()
  public controlName: string;

  /**
   * @description root form group
   */
  @Input()
  public parentGroup: FormGroup;

  /**
   * @description type of input field
   */
  @Input()
  public type = 'text';

  /**
   * @description placeholder of input
   */
  @Input()
  public placeholder = '';

  /**
   * @description class name for valid/invalid state of form control
   */
  private _controlClass = '';
  /**
   * @description label name for invalid state of form control
   */
  private _errorLabels = [];

  /**
   * @description class name getter
   */
  public get controlClass() {
    return this._controlClass;
  }
  /**
   * @description label name getter
   */
  public get errorLabels() {
    return this._errorLabels.map(err => err);
  }

  ngOnInit() {
    this.handleControlStatusChange();
  }

  /**
   * @description set class name and labes of form control
   * @param controlName formControlName
   */
  private handleControlValidity(controlName: string) {
    const control = this.parentGroup.get(controlName);

    if (!control.valid && control.dirty) {
      this._controlClass = 'is-danger';
      for (let error in control.errors) {
        if (ValidationLabels[error.toUpperCase()]) {
          this._errorLabels.push(ValidationLabels[error.toUpperCase()]);
        }
      }
    }
    if (control.valid && control.dirty) {
      this._controlClass = 'is-success';
      this._errorLabels = [];
    }
  }

  private handleControlStatusChange() {
    const control = this.parentGroup.get(this.controlName);
    control.statusChanges.pipe(distinctUntilChanged()).subscribe(_ => {
      this.handleControlValidity(this.controlName);
    });
  }
}
