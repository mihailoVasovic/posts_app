import { Component } from '@angular/core';

import { AbstractInputComponent } from '../abstract-input.component';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss']
})
export class TextareaComponent extends AbstractInputComponent {
  
  
  constructor() {
    super()
  }
}
