import { Store } from '@ngrx/store';

export abstract class AbstractListComponent {
  protected list$;

  public page = 1;

  protected take = 10;

  constructor() {}

  protected abstract fetchListItems(page);
}
