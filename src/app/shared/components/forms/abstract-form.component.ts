import { FormGroup } from '@angular/forms';
export abstract class AbstractFormComponent {
  /**
   * @description FormGroup
   */
  protected _form: FormGroup;

  /**
   * @description form getter
   */
  protected get form() {
    return this._form as FormGroup;
  }

  public abstract initializeForm();
  public abstract submitForm();
}
