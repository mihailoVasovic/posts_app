import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Output()
  public clickEvent: EventEmitter<any> = new EventEmitter();

  /**
   * @description name of button
   */
  @Input()
  public placeholder = '';

  /**
   * @description state of button
   */
  @Input()
  public disabled = false;

  /**
   * @description list of strings which are representing class names
   * @type string[]
   */
  @Input()
  public classList;

  constructor() {}

  ngOnInit() {}

  /**
   * @description emit click event
   */
  public handleClick() {
    this.clickEvent.emit();
  }
}
