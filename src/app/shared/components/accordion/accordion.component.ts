import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../../../store/index';
import { Post } from './../../models/Post';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {
  /**
   * @description contains Post object
   */
  @Input()
  public post: Post;

  /**
   * @description index of item in list
   * @type number
   */
  @Input()
  public index;

  constructor(private store: Store<any>) {}

  ngOnInit() {}

  /**
   * @description handles post remove functionality
   * @param id id of post
   */
  public handleRemoveEvent(id: string) {
    const dialogResult = confirm(
      'Are you sure that you want to delete this post?'
    );
    if (dialogResult) {
      this.store.dispatch(new fromStore.Remove(id));
    }
  }
}
