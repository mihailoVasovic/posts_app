import { ApiService } from './../core/api/api.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InputComponent } from './components/inputs/input/input.component';
import { TextareaComponent } from './components/inputs/textarea/textarea.component';
import { AccordionComponent } from './components/accordion/accordion.component';
import { InfinityScrollDirective } from './directives/scroll.directive';
import { ButtonComponent } from './components/buttons/button.component';
import { RouterModule } from '@angular/router';
import { LoaderComponent } from './components/loader/loader.component';
import { FilterComponent } from './components/inputs/filter/filter.component';

const COMPONENTS = [
  InputComponent,
  TextareaComponent,
  AccordionComponent,
  InfinityScrollDirective,
  ButtonComponent,
  LoaderComponent,
  FilterComponent
];
@NgModule({
  declarations: [...COMPONENTS],
  imports: [CommonModule, ReactiveFormsModule, HttpClientModule, RouterModule],
  exports: [...COMPONENTS, ReactiveFormsModule],
  providers: [ApiService]
})
export class SharedModule {}
