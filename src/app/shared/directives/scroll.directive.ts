import {
  Directive,
  Output,
  EventEmitter,
  ElementRef,
  OnInit
} from '@angular/core';
import { fromEvent } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

@Directive({
  selector: '[InfiniteScroll]'
})
export class InfinityScrollDirective implements OnInit {
  @Output()
  public heightEmitter = new EventEmitter();

  constructor(private el: ElementRef) {}

  ngOnInit() {
    this.handleScroll();
  }
  public handleScroll() {
    const element = this.el.nativeElement;
    fromEvent(element, 'scroll')
      .pipe(
        map(
          () =>
            element.scrollTop === element.scrollHeight - element.offsetHeight
        ),
        distinctUntilChanged()
      )
      .subscribe(e => {
        this.heightEmitter.emit(e);
      });
  }
}
