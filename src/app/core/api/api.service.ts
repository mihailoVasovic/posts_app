import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

const URL = 'http://localhost:3000/posts';

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) {}

  /**
   * delay operator added for response delay simulation
   */

  public fetchPosts(payload: {
    page: string;
    searchValue: string;
  }): Observable<Object> {
    const params = {
      _page: payload.page.toString(),
      _limit: '10',
      title_like: payload.searchValue ? payload.searchValue : ''
    };

    return this.http.get(`${URL}`, { params }).pipe(delay(700));
  }

  public addPost(payload: { title: string; body: string }): Observable<Object> {
    return this.http
      .post(`${URL}`, {
        title: payload.title,
        userId: 11,
        body: payload.body
      })
      .pipe(delay(700));
  }

  public editPost(payload: {
    id: string;
    title: string;
    body: string;
  }): Observable<Object> {
    return this.http
      .patch(`${URL}/${payload.id}`, {
        title: payload.title,
        body: payload.body
      })
      .pipe(delay(700));
  }

  public fetchPost(payload: string): Observable<Object> {
    return this.http.get(`${URL}/${payload}`).pipe(delay(700));
  }

  public removePost(payload: string): Observable<Object> {
    return this.http.delete(`${URL}/${payload}`).pipe(delay(700));
  }
}
