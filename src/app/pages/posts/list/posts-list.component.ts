import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  AfterViewInit
} from '@angular/core';
import { of, fromEvent } from 'rxjs';
import { debounceTime, map, distinctUntilChanged, tap } from 'rxjs/operators';
import { AbstractListComponent } from 'src/app/shared/components/list/abstract-list.component';
import { Store } from '@ngrx/store';
import * as fromStore from '../../../store/index';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss']
})
export class PostsListComponent extends AbstractListComponent
  implements OnInit, AfterViewInit {
  /**
   * @description value from filter input
   */
  private searchValue;

  /**
   * @description container ref of filter input
   */
  @ViewChild('searchInput', { static: true, read: ViewContainerRef })
  private searchInput: ViewContainerRef;

  constructor(private store: Store<any>) {
    super();
  }

  ngOnInit() {
    this.list$ = this.store.select(fromStore.posts);

    this.fetchListItems(1);
  }

  ngAfterViewInit(): void {
    this.handleSearch();
  }

  /**
   * @description fetch list items from db
   * @param page number of page that should be fetched
   */
  protected fetchListItems(page?: number) {
    this.store.dispatch(new fromStore.Fetch({ page }));
  }

  /**
   * @description handles infinite scroll functionality
   * @param shouldFetchData check if filter is enabled
   */
  public handleInfiniteScroll(shouldFetchData) {
    if (shouldFetchData) {
      if (this.searchValue) {
        this.store.dispatch(
          new fromStore.Search({
            page: this.page,
            searchValue: this.searchValue
          })
        );
      } else {
        this.store.dispatch(new fromStore.Fetch({ page: this.page }));
      }
      this.page++;
    }
  }

  /**
   * @description handles filter functionality
   */
  public handleSearch() {
    fromEvent(this.searchInput.element.nativeElement, 'keyup')
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        map(el => el['target'].value)
      )
      .subscribe(val => {
        this.store.dispatch(new fromStore.Reset());
        this.page = 1;
        val
          ? this.store.dispatch(
              new fromStore.Search({ page: this.page, searchValue: val })
            )
          : this.store.dispatch(new fromStore.Fetch({ page: this.page }));

        this.searchValue = val;
        this.page++;
      });
  }
}
