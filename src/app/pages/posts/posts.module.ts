import { EditComponent } from './edit/edit.component';
import { PostsRoutingModule } from './posts-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsComponent } from './_container/posts.component';
import { PostsListComponent } from './list/posts-list.component';
import { SharedModule } from 'src/app/shared/shared.module';

const COMPONENTS = [PostsComponent, PostsListComponent, EditComponent];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [CommonModule, SharedModule, PostsRoutingModule],
  exports: [],
  providers: []
})
export class PostsModule {}
