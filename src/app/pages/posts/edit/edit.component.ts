import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Params } from '@angular/router';

import { Store } from '@ngrx/store';

import { AbstractFormComponent } from 'src/app/shared/components/forms/abstract-form.component';
import { Post } from './../../../shared/models/Post';
import * as fromStore from '../../../store/index';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent extends AbstractFormComponent implements OnInit {
  /**
   * @type string
   * @description check if page should be in add or edit mode
   */
  private isNewEntity;

  constructor(
    private fb: FormBuilder,
    private store: Store<any>,
    private router: Router,
    private route: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      this.isNewEntity = params.isNewEntity;
      if (params.isNewEntity === 'false') {
        this.store.dispatch(
          new fromStore.FetchOne(this.route.snapshot.paramMap.get('id'))
        );

        this.store.select(fromStore.post).subscribe((post: Post) => {
          this.initializeForm(post);
        });
      } else {
        this.initializeForm();
      }
    });
  }

  /**
   * @description populates form with value from db if page is in edit mode,
   *              or populates with empty strings if page is in add mode
   * @param payload title: string, body: string
   */
  public initializeForm(payload?) {
    this._form = this.fb.group({
      title: [payload ? payload.title : '', Validators.required],
      body: [payload ? payload.body : '', Validators.required]
    });
  }

  /**
   * @description edit post if in edit mode or add new one if in add mode
   */
  public submitForm() {
    this.isNewEntity === 'true'
      ? this.store.dispatch(new fromStore.Add(this.form.getRawValue()))
      : this.store.dispatch(
          new fromStore.Edit({
            ...this.form.getRawValue(),
            id: this.route.snapshot.paramMap.get('id')
          })
        );
  }

  public handleBack() {
    this.router.navigate(['/list']);
  }
}
