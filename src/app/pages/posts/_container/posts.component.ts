import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../../../store/index';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  /**
   * @type Observable<boolean>
   * @description show loading indicator if true
   */
  public loading$;

  constructor(private store: Store<any>) {}

  ngOnInit() {
    /**
     * @description get loading from store
     */
    this.loading$ = this.store.select(fromStore.loading);
  }
}
