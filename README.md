# PostsApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.

## Development server

Run `npm run start:mock:server` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Description

Application should contain list of user posts. User can edit or remove any post. Also, user can add new post.
Infinity scroll and filter should be added in application. Make sure that application contains form for adding new post,
or edit existing one and add validations.
Implement lazy loading and loading indicator.

## Technology

Application was built using Angular framework with Ngrx state management library.
Json-server is used as fake REST API. Bulma is used as CSS framework.
